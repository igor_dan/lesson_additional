//Как по мне самый оптимальный вариант использования, так как интуитивно понятно что происходит.
import pkg from 'gulp';

import fileInclude from 'gulp-file-include'; // Подключение html секций
import browserSync from 'browser-sync'; //Локальный сервер что бы сразу видить свои изменения
const {task, watch, series, src, dest, parallel} = pkg;

/**
 * Пакеты для обоработки стилей
 * */
import csscomb from 'gulp-csscomb';  //Сортирует css свойства
import csso from 'gulp-csso';  //Оптимизация css
import gulpPostcss from 'gulp-postcss'; //Пост обработка файлов css
import postcss from 'postcss'; //Пост обработка файлов css
import gulpSass from 'gulp-sass'; //Обработка sass/scss файлов, и перекомпиляция в css
import dartSass from 'sass'; //Обработка sass/scss файлов
const sass = gulpSass(dartSass);
import cssnano from 'cssnano'; //Минификация стилей
import flexFix from 'postcss-flexbugs-fixes'; //Фикс частых ошибок при использовании FlexBox
import sassInheritance from 'gulp-sass-inheritance'; //Проверяет зависимости в файлах scss
import groupmedia from 'gulp-group-css-media-queries'; // Групируем media-queries
import sourcemaps from 'gulp-sourcemaps'; //Создает карту css свойст
import autoprefixer from 'autoprefixer'; //Добавление поефикса(флага) для свойст если нужно

/**
 * Вспомогательные пакеты
 * */
import notify from 'gulp-notify'; // Подключение уведомления об ошибках в файле
import plumber from 'gulp-plumber'; // Подключение уведомления об ошибках в файле
import rename from 'gulp-rename'; // Переименование файла
import debug from 'gulp-debug'; // Вспомогательные сообщения в консоле
import { deleteAsync } from 'del'; // Удаление файлов
import chalk from 'chalk'; // Редактирование текста в консоле

task('styleCSS', () => {
	const processors = [
		// auto fix some flex-box issues
		flexFix(),
		// auto adds vendor prefixes
		autoprefixer({
			grid: true,
			cascade: true
		})
	];

	return src("./src/styles/**/*.scss")
		.pipe(
			plumber({
				errorHandler: notify.onError(err => ({
					title: "sass",
					message: err.message
				}))
			})
		) // Window notification
		.pipe(sourcemaps.init())
		.pipe(sassInheritance({dir: './src/styles/'})) //Проверяет зависимости в файлах scss
		.pipe(sass.sync({
			sourceComments: false,
			outputStyle: "expanded"
		}).on('error', sass.logError)) /// scss => css  =>  style.css
		.pipe(groupmedia()) //групируем медия выражения
		.pipe(debug({title: chalk.cyan.bold(`-> Start style minification`)}))
		.pipe(gulpPostcss(processors)) //делаем проверку стилей на ошибки flexBox и добавляем браузерную приставку
		.pipe(csscomb()) // Format CSS coding style with
		.pipe(csso({
			restructure: true,
			sourceMap: true,
			debug: false
		})) // делаем проверку стилей  // => style.css
		.pipe(gulpPostcss([cssnano]))  // делаем минификацию файлов // => style.css
		.pipe(rename({suffix: ".min"})) // переименовываем файл // => min.style.css
		.pipe(sourcemaps.write('.')) // => min.style.css
		.pipe(plumber.stop())
		.pipe(dest("./dist/css/"))
		.pipe(browserSync.stream());
})

task('moveHTML', () => {
	return src("./src/*.html")
	.pipe(fileInclude()) 
	.pipe(dest("./dist"))
	.pipe(browserSync.stream());
})

task('moveIMG', () => {
	return src("./src/images/**/*.*")
		.pipe(dest("./dist/images/"));
})

task('clean', () => {
	return deleteAsync(["./dist"]); // удаляет папку dist
});

task('serve', () => {
	return browserSync.init({
		server: {
			baseDir: ['dist']
		},
		port: 9000,
		open: true
	});
});
task('watchers', () => {
	watch('./src/styles/**/*.scss', parallel('styleCSS')).on('change', browserSync.reload);
	watch('./src/views/*.html', parallel('moveHTML')).on('change', browserSync.reload);
});

task('dev', series('clean', 'styleCSS', 'moveHTML', 'moveIMG')) // gulp dev

task('default', series('dev', parallel('serve', 'watchers'))); // gulp


